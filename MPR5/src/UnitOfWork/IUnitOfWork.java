package UnitOfWork;


import domain.Ent;


public interface IUnitOfWork {

    public void commit();
    public void rollback();
    public void markAsNew(Ent ent, UnitOfWorkRepository repository);
    public void markAsDirty(Ent ent, UnitOfWorkRepository repository);
    public void markAsDeleted(Ent ent, UnitOfWorkRepository repository);
}