package UnitOfWork;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.Map;

import domain.Ent;
import domain.EntState;
import domain.Entity;
import domain.EntityState;

public class UnitOfWork implements IUnitOfWork{


    private Connection connection;

    private Map<Ent, UnitOfWorkRepository> entities =
            new LinkedHashMap<Ent, UnitOfWorkRepository>();

    public UnitOfWork(Connection connection) {
        super();
        this.connection = connection;

        try {
            connection.setAutoCommit(false);

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void commit() {

        for(Ent ent: ent.keySet())
        {
            switch(ent.getState())
            {
                case Modified:
                    ent.get(ent).persistUpdate(ent);
                    break;
                case Deleted:
                    ent.get(ent).persistDelete(ent);
                    break;
                case New:
                    ent.get(ent).persistAdd(ent);
                    break;
                case UnChanged:
                    break;
                default:
                    break;}
        }

        try {
            connection.commit();
            entities.clear();

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void rollback() {

        entities.clear();

    }

    @Override
    public void markAsNew(Ent ent, UnitOfWorkRepository repository) {
        ent.setState(EntState.New);
        entities.put(ent, repository);

    }

    @Override
    public void markAsDirty(Ent entity, UnitOfWorkRepository repository) {
        entity.setState(EntState.Modified);
        entities.put(entity, repository);

    }

    @Override
    public void markAsDeleted(Ent ent, UnitOfWorkRepository repository) {
        ent.setState(EntState.Deleted);
        entities.put(ent, repository);

    }

}