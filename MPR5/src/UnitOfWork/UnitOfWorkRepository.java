package UnitOfWork;

import domain.Ent;

public interface UnitOfWorkRepository {

    public void persistAdd(Ent ent);
    public void persistUpdate(Ent ent);
    public void persistDelete(Ent ent);
}
