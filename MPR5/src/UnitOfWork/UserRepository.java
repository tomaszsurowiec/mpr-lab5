package UnitOfWork;

import domain.User;




public interface UserRepository extends Repository<User>  {
    public User withLogin(String login);
}
