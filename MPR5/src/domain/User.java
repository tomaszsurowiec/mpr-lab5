package domain;

public class User extends Ent{

    private String login;
    private String password;
    private int idx_person;
    private Person person;


    public User(String login, String password) {
        this.login = login;
        this.password = password;
    }

    public int getId() {
        return super.getId();
    }


    public void setId(int id) {
        super.setId(id);
    }


    public int getIdx_person() {
        return idx_person;
    }

    public void setIdx_person(int idx_person) {
        this.idx_person = idx_person;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    @Override
    public String toString() {
        return "User{" +
                "login='" + login + '\'' +
                ", password='" + password + '\'' +
                ", person=" + person +
                '}';
    }

}
