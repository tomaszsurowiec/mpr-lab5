package domain;



public class Permission extends  Ent{

    private String permission;

    public Permission(String permission) {
        this.permission = permission;
    }

    public int getId() {
        return super.getId();
    }

    public void setId(int id) {
        super.setId(id);
    }

    public String getPermission() {
        return permission;
    }

    public void setPermission(String permission) {
        this.permission = permission;
    }
}