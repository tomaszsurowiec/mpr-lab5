package domain;



	public class RolePermission extends Ent{
	    private int idx_role;
	    private int idx_permission;


	    public RolePermission( int idx_role, int idx_permission) {
	        this.idx_role = idx_role;
	        this.idx_permission = idx_permission;
	    }
	    public int getId() {
	        return super.getId();
	    }

	    public void setId(int id) {
	        super.setId(id);
	    }

	    public int getIdx_role() {
	        return idx_role;
	    }

	    public void setIdx_role(int idx_role) {
	        this.idx_role = idx_role;
	    }

	    public int getIdx_permission() {
	        return idx_permission;
	    }

	    public void setIdz_permission(int idx_permission) {
	        this.idx_permission = idx_permission;
	    }
	}

