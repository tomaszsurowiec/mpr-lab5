package Catalog;

import Repozytoria.HsqlPermissionRepository;
import Repozytoria.HsqlPersonRepository;
import Repozytoria.HsqlRolePermissiomRepository;
import Repozytoria.HsqlRoleRepository;
import UnitOfWork.PermissionRepository;
import UnitOfWork.PersonRepository;
import UnitOfWork.Repository;
import UnitOfWork.RepositoryCatalog;
import UnitOfWork.RolePermissionRepository;

import com.sun.corba.se.pept.transport.Connection;


	public class HsqlRepositoryCatalog implements RepositoryCatalog {

	    Connection connection;

	    public HsqlRepositoryCatalog(Connection connection) {
	        this.connection = connection;
	    }

	    public PersonRepository people() {
	        return new HsqlPersonRepository(connection);
	    }

	    public Repository role() {
	        return new HsqlRoleRepository(connection);
	    }

	    public PermissionRepository permission() {
	        return new HsqlPermissionRepository(connection);
	    }

	    public AddressRepository address() {
	        return new HsqlAddressRepository(connection);
	    }

	    public UserRepository user() {
	        return new HsqlUserRepository(connection);
	    }

	    public RolePermissionRepository rolePerm() {
	        return new HsqlRolePermissiomRepository(connection);
	    }

	}

